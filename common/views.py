from django.shortcuts import render
from gallery.models import Gallery
from blog.models import Blog
from article.models import Article
from datetime import datetime
from catalogue.models import Category
# Create your views here.


def home(request):
    data = {
        "categories": Category.objects.filter(active=True),
        "blogs": Blog.objects.filter(active=True)[:5],
        "galleries": Gallery.objects.filter(active=True, date__lte=datetime.now())[:5],
        "articles": Article.objects.filter(active=True)[:10],
    }

    return render(request, "home.html", data)