from django import template
from django.conf import settings

register = template.Library()

@register.inclusion_tag('common/language_selector.html')
def language_selector():
    return {'languages': settings.LANGUAGES}