from django.db import models

# Create your models here.
class Blog(models.Model):
    name = models.CharField(max_length=200)

    active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name