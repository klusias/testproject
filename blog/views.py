from django.shortcuts import render
from catalogue.models import Category


# Create your views here.
def blog(request):
    data = {
        "categories": Category.objects.filter(active=False),
    }
    return render(request, "blog/index.html", data)