from django.db import models
from datetime import datetime

# Create your models here.
class Gallery(models.Model):
    name = models.CharField(max_length=200)

    active = models.BooleanField(default=False)
    date = models.DateTimeField(default=datetime.now)
    
    def __unicode__(self):
        return self.name