from django.shortcuts import render
from catalogue.models import Category


# Create your views here.
def article(request):
    data = {
        "categories": Category.objects.filter(active=True).order_by('?'),
    }
    return render(request, "article/index.html", data)